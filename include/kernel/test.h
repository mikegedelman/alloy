#ifndef TEST_CPP_H
#define TEST_CPP_H

#ifdef __cplusplus
#define EXTERNC extern "C"
#else
#define EXTERNC
#endif
 
EXTERNC void cpptest();

#undef EXTERNC

#endif