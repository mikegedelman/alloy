# alloy

A toy OS used for my learning.

## Linux requirements

```bash
sudo apt install build-essential nasm texinfo libgmp-dev libmpfr-dev libmpc-dev qemu-system-i386
```

optional: `direnv`; without it, manually set your PATH include $HOME/opt/cross/bin

## Building

1. Run `scripts/build-elf-toolchain.sh` to build a os-specific toolchain and save it in $HOME/opt/cross/bin

    - This is sufficient to build and run the kernel as-is. However, if you also want to build userspace programs and run them, the following two steps are also required.

2. Run `scripts/build-newlib.sh` to build newlib

3. Run `build-hosted-toolchain.sh` to build the self-hosted toolchain.