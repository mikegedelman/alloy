extern "C" {
#include <kernel/all.h>
}
#include <kernel/test.h>

class Bear {
public:
    void say() {
        puts("bear says hi\n");
    }
};

void cpptest() {
    Bear b;
    b.say();
}